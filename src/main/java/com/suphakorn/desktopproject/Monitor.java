/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.desktopproject;

/**
 *
 * @author exhau
 */
public class Monitor {

    private String Name;

    public Monitor(String Name) {
        this.Name = Name;
        System.out.println("Monitor name is " + Name);
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
        System.out.println("Monitor changed name to " + Name);
    }

}
